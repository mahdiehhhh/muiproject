import { Button, IconButton, Stack, Typography } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import SendIcon from "@mui/icons-material/Send";
import AlarmIcon from "@mui/icons-material/Alarm";
import LoadingButton from "@mui/lab/LoadingButton";
import Fingerprint from "@mui/icons-material/Fingerprint";
const MuiButton = () => {
  return (
    <>
      <Stack
        direction='row'
        spacing={2}>
        <Button variant='contained'>contained</Button>
        <Button variant='outlined'>outlined</Button>
        <Button
          variant='contained'
          disabled>
          disabled
        </Button>
        <Button
          variant='contained'
          href='/'>
          link
        </Button>
        <Button
          variant='contained'
          disableElevation>
          disableElevation
        </Button>
        <Button color='secondary'>secondary</Button>
        <Button
          variant='contained'
          color='success'>
          success
        </Button>
        <Button
          variant='outlined'
          color='error'>
          Error
        </Button>

        <Button
          variant='outlined'
          size='small'>
          small
        </Button>

        <Button
          variant='outlined'
          size='medium'>
          medium
        </Button>

        <Button
          variant='outlined'
          size='large'>
          large
        </Button>

        <Button
          variant='contained'
          size='large'
          startIcon={<DeleteIcon />}>
          DeleteIcon
        </Button>
        <Button
          variant='contained'
          size='large'
          endIcon={<SendIcon />}>
          SendIcon
        </Button>
      </Stack>
      <Stack
        direction='row'
        spacing={2}>
        <Typography variant='h3'>IconButton</Typography>
        <IconButton
          color='secondary'
          aria-label='AlarmIcon'>
          <AlarmIcon />
        </IconButton>
        <IconButton
          color='secondary'
          aria-label='fingerprint'>
          <Fingerprint />
        </IconButton>
      </Stack>
      <Stack
        direction='row'
        spacing={2}>
        <Typography variant='h3'>Loading button</Typography>
        <LoadingButton
          loading
          variant='contained'
          size='small'></LoadingButton>
        <LoadingButton
          loading
          loadingIndicator='Loading.....'
          variant='outline'
          color='secondary'
          size='small'></LoadingButton>
        <LoadingButton
          loading
          loadingPosition='start'
          variant='contained'
          color='secondary'
          startIcon={<SendIcon />}
          size='small'>
          send
        </LoadingButton>
      </Stack>
    </>
  );
};

export default MuiButton;
