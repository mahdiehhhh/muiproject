import { Stack, TextField } from "@mui/material";

const MuiTextField = () => {
  return (
    <>
      <Stack spacing={2} direction="row">
        <TextField id="outlined-basic" label="Outlined" variant="outlined" />
        <TextField id="filled-basic" label="filled" variant="filled" />
        <TextField id="standard-basic" label="standard" variant="standard" />
        <TextField
          id="password-basic"
          label="password"
          variant="filled"
          type="password"
        />
        <TextField
          required
          id="outlined-required"
          label="Required"
          defaultValue="Hello World"
        />
        <TextField
          disabled
          id="outlined-disabled"
          label="disabled"
          defaultValue="Hello World"
        />
        <TextField
          inputProps={{ readOnly: true }}
          id="outlined-readOnly"
          label="readOnly"
          defaultValue="Hello World readOnly"
        />
      </Stack>

      <Stack spacing={2} direction="row">
        <TextField
          id="number-basic"
          label="number"
          variant="standard"
          type="number"
        />
        <TextField
          id="search-basic"
          label="search"
          variant="standard"
          type="search"
        />
        <TextField
          error
          id="search-basic"
          label="search"
          variant="standard"
          type="search"
        />
      </Stack>
    </>
  );
};

export default MuiTextField;
