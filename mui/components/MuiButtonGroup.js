import { Button, ButtonGroup, Stack } from "@mui/material";

const MuiButtonGroup = () => {
  return (
    <Stack>
      <ButtonGroup
        aria-label='ButtonGroup'
        variant='outlined'>
        <Button>One</Button>
        <Button>Two</Button>
        <Button>Three</Button>
      </ButtonGroup>
      <ButtonGroup
        aria-label='ButtonGroup'
        variant='text'
        color='secondary'
        size='small'
        sx={{ marginTop: "20px" }}>
        <Button>One</Button>
        <Button>Two</Button>
        <Button>Three</Button>
      </ButtonGroup>
      <ButtonGroup
        aria-label='ButtonGroup'
        variant='contained'
        color='secondary'
        orientation='vertical'
        size='small'
        sx={{ marginTop: "20px" }}>
        <Button>One</Button>
        <Button>Two</Button>
        <Button>Three</Button>
      </ButtonGroup>
    </Stack>
  );
};

export default MuiButtonGroup;
