import { FormControlLabel, Radio, RadioGroup, Stack } from "@mui/material";
import { useState } from "react";

const MuiRadio = () => {
  const [gender, setGender] = useState(null);
  const g = ["femail", "male", "other"];
  const genderHandler = (e) => {
    setGender(e.target.value);
  };
  console.log("gender", gender);
  return (
    <Stack spacing={2} direction="row">
      <RadioGroup defaultValue="female" value={gender} onChange={genderHandler}>
        {g.map((item) => (
          <FormControlLabel
            key={item}
            value={item}
            control={<Radio />}
            label={item}
          />
        ))}
      </RadioGroup>
    </Stack>
  );
};

export default MuiRadio;
