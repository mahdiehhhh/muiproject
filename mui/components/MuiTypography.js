import { Stack, Typography } from "@mui/material";

const MuiTypography = () => {
  return (
    <Stack sx={{ textAlign: "center" }}>
      <Typography
        variant='h1'
        component='h4'>
        h1
      </Typography>
      <Typography
        variant='h1'
        component='h4'>
        h1
      </Typography>
      <Typography
        variant='h2'
        component='h2'>
        h2
      </Typography>
      <Typography
        variant='h3'
        component='h3'>
        h3
      </Typography>
      <Typography
        variant='h4'
        component='h4'>
        h4
      </Typography>
      <Typography
        variant='h5'
        component='h5'>
        h5
      </Typography>
      <Typography
        variant='h6'
        component='h6'>
        h6
      </Typography>
      <Typography
        variant='subtitle1'
        component='span'>
        subtitle1
      </Typography>
      <Typography
        variant='subtitle2'
        component='span'>
        subtitle2
      </Typography>
      <Typography variant='body1'>
        {" "}
        body1. Lorem ipsum dolor sit amet, consectetur adipisicing elit
      </Typography>
      <Typography variant='body2'>
        {" "}
        body2. Lorem ipsum dolor sit amet, consectetur adipisicing elit
      </Typography>
    </Stack>
  );
};

export default MuiTypography;
