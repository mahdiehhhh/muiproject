import { CheckBox } from "@mui/icons-material";
import {
  Box,
  Checkbox,
  Chip,
  FormControl,
  InputLabel,
  ListItemText,
  MenuItem,
  OutlinedInput,
  Select,
  Stack,
} from "@mui/material";
import { useState } from "react";
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};
const MuiSelect = () => {
  const names = [
    "Oliver Hansen",
    "Van Henry",
    "April Tucker",
    "Ralph Hubbard",
    "Omar Alexander",
    "Carlos Abbott",
    "Miriam Wagner",
    "Bradley Wilkerson",
    "Virginia Andrews",
    "Kelly Snyder",
  ];
  const [age, setAge] = useState(null);
  const [person, setPerson] = useState([]);
  const handelChange = (e) => {
    setAge(e.target.value);
  };

  const personChange = (e) => {
    const {
      target: { value },
    } = e;
    setPerson(typeof value === "string" ? value.split(",") : value);
  };
  return (
    <Stack spacing={2} direction="row">
      <FormControl>
        <InputLabel id="demo-simple-select-label">Age</InputLabel>
        <Select value={age} label="Age" onChange={handelChange}>
          <MenuItem value={1}>1</MenuItem>
          <MenuItem value={2}>2</MenuItem>
          <MenuItem value={3}>3</MenuItem>
        </Select>
      </FormControl>

      <FormControl variant="standard">
        <InputLabel id="demo-simple-select-label">Age</InputLabel>
        <Select value={age} label="Age" onChange={handelChange}>
          <MenuItem value={1}>1</MenuItem>
          <MenuItem value={2}>2</MenuItem>
          <MenuItem value={3}>3</MenuItem>
        </Select>
      </FormControl>

      <FormControl variant="filled">
        <InputLabel id="demo-simple-select-label">Age</InputLabel>
        <Select value={age} label="Age" onChange={handelChange}>
          <MenuItem value={1}>1</MenuItem>
          <MenuItem value={2}>2</MenuItem>
          <MenuItem value={3}>3</MenuItem>
        </Select>
      </FormControl>

      <FormControl error>
        <InputLabel id="demo-simple-select-label">Age</InputLabel>
        <Select
          value={age}
          label="Age"
          onChange={handelChange}
          renderValue={(value) => `⚠️  - ${value}`}
        >
          <MenuItem value={1}>1</MenuItem>
          <MenuItem value={2}>2</MenuItem>
          <MenuItem value={3}>3</MenuItem>
        </Select>
      </FormControl>

      {/* multi select  */}

      <FormControl>
        <InputLabel id="demo-simple-select-label">Age</InputLabel>
        <Select
          multiple
          value={person}
          onChange={personChange}
          renderValue={(selected) => (
            <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
              {selected.map((value) => (
                <Chip key={value} label={value} />
              ))}
            </Box>
          )}
        >
          {names.map((item) => (
            <MenuItem value={item}>{item}</MenuItem>
          ))}
        </Select>
      </FormControl>

      <FormControl>
        <InputLabel id="demo-simple-select-label">Age</InputLabel>
        <Select
          multiple
          value={person}
          onChange={personChange}
          renderValue={(selected) => selected.join(", ")}
          input={<OutlinedInput label="Tag" />}
          MenuProps={MenuProps}
        >
          {names.map((item) => (
            <MenuItem key={item} value={item}>
              <Checkbox checked={person.indexOf(item) > -1} />
              <ListItemText primary={item} />
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Stack>
  );
};

export default MuiSelect;
