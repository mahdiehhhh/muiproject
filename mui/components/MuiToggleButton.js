import { Stack, ToggleButton, ToggleButtonGroup } from "@mui/material";
import { useState } from "react";
import FormatBoldIcon from "@mui/icons-material/FormatBold";
import FormatItalicIcon from "@mui/icons-material/FormatItalic";
import FormatUnderlinedIcon from "@mui/icons-material/FormatUnderlined";
import LaptopIcon from "@mui/icons-material/Laptop";
import TvIcon from "@mui/icons-material/Tv";
import PhoneAndroidIcon from "@mui/icons-material/PhoneAndroid";
const MuiToggleButton = () => {
  const [toggle, setToggle] = useState([]);
  const [toggleMulti, setToggleMulti] = useState(["TvIcon"]);
  const handelToggle = (e, updateToggle) => {
    setToggle(updateToggle);
  };

  //   const handelmultiToggle = (event,new) => {
  //     if(toggleMulti !== null){
  //         setToggleMulti(new)
  //     }
  //   };
  const handelmultiToggle = (e, newToggle) => {
    if (newToggle.length) {
      setToggleMulti(newToggle);
    }
  };
  return (
    <>
      <Stack>
        <ToggleButtonGroup
          value={toggle}
          onChange={handelToggle}
          aria-label='ToggleButtonGroup'
          //   orientation='vertical'
          exclusive>
          <ToggleButton value='bold'>
            <FormatBoldIcon />
          </ToggleButton>
          <ToggleButton value='italic'>
            <FormatItalicIcon />
          </ToggleButton>
          <ToggleButton value='underLine'>
            <FormatUnderlinedIcon />
          </ToggleButton>
        </ToggleButtonGroup>
      </Stack>
      <Stack>
        <ToggleButtonGroup
          value={toggleMulti}
          onChange={handelmultiToggle}>
          <ToggleButton value='LaptopIcon'>
            <LaptopIcon />
          </ToggleButton>

          <ToggleButton value='TvIcon'>
            <TvIcon />
          </ToggleButton>

          <ToggleButton value='PhoneAndroidIcon'>
            <PhoneAndroidIcon />
          </ToggleButton>
        </ToggleButtonGroup>
      </Stack>
    </>
  );
};

export default MuiToggleButton;
